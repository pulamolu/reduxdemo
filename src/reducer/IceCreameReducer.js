import ACTION from '../actions/actionTypes'


const initialState = {
    numOfIceCreams : 20
}

const IceCreameReducer= (state = initialState,action) => {
    switch(action.type)
    {
        case ACTION.BUY_ICE_CREAM :
            return {
                ...state,
                numOfIceCreams : state.numOfIceCreams -1
            }
        default :
            return state
    }
}

export default IceCreameReducer