import {createStore} from 'redux'
import IceCreameReducer from '../reducer/IceCreameReducer'

const store = createStore(IceCreameReducer)
export default store 