import ACTION from './actionTypes'

const buyIceCream = () => {
    return {
        type : ACTION.BUY_ICE_CREAM
    }
}

export default buyIceCream