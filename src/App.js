import React from 'react';
import IceCreamContainer from './components/IceCreamContainer'
import './App.css';
import store from './store/store'
import {Provider} from 'react-redux'


function App() {
  return (
    <Provider store={store}>
      <div>
        <IceCreamContainer />
      </div>
    </Provider>
  );
}

export default App;
