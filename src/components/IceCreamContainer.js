import React, { Component } from 'react'
import {connect} from 'react-redux'
import buyIceCream from '../actions/actionCreators'

class IceCreamContainer extends Component {
    render() {
        return (
            <div>
                <h1>The No of IceCreames : {this.props.numOfIceCreams}     </h1>
                <button onClick = {this.props.buyIceCream}>Buy IceCreame</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        numOfIceCreams : state.numOfIceCreams
    }
}

const mapDispatchToProps = dispatch => {
    return{
        buyIceCream : () => dispatch(buyIceCream())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(IceCreamContainer)
